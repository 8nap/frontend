import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
class NodeOfClass {
  constructor(id, node, contentEditable, backgroundColor) {
    this.node = node;
    this.contentEditable = contentEditable;
    this.backgroundColor = backgroundColor;
    this.id = id;
  }
  static number = 0;
  id: string;
  node: string;
  contentEditable: boolean;
  backgroundColor: string;
}

@Component({
  selector: 'app-start',
  templateUrl: './start.component.html',
  styleUrls: ['./start.component.css'],
  changeDetection: ChangeDetectionStrategy.Default
})
export class StartComponent implements OnInit, OnChanges {
  mylist: NodeOfClass[];
  highlighted: any = null;
  stringList: string[] = [];
  exBackendURL: string;

  private getList(): Observable<string[]> {
    return this.httpClient.get <string[]>(this.exBackendURL);
  }

  constructor(private httpClient: HttpClient, private ref: ChangeDetectorRef) {
  }

  ngOnInit() {
    this.exBackendURL = environment.backend + '/hello';
    this.updateView();
  }
  ngOnChanges(changes: SimpleChanges): void {
    this.ref.detectChanges();
  }

  addNew() {
    if (this.highlighted === null) {
      this.mylist.push(new NodeOfClass('new Element', 'new Element', true, 'red'));
      this.highlight('new Element');
      this.highlighted = 'new Element';
    }
  }
  testfunc(id): boolean {
    return id !== 'Edit Element' && id !== 'new Element';
  }
  testfunc1(id): boolean {
    return !id;
  }
  cancelAction(id) {
    if (id === 'new Element') {
      this.mylist.pop();
      NodeOfClass.number--;
      this.highlighted = null;
    }

  }
  confirmAction(id) {
      if (id === 'new Element') {
        {
          this.addElement();
        }
      }
      this.highlighted = null;
    }

  private updateView() {
    this.mylist = [];
    NodeOfClass.number = 0;
    this.ref.detectChanges();
    this.getList().subscribe(
      x => {
        this.stringList = x;
        this.stringList.map(x1 => {
          this.mylist.push(new NodeOfClass(NodeOfClass.number++, x1, false, 'white'));
        });
        console.log(this.mylist);
      }
    );
  }
  addElement() {
    this.addNewElement().subscribe();
    this.updateView();
  }
  addNewElement(): Observable<string> {
    const newElem = document.getElementById('new Element');
    const newName = newElem.innerText;
    const postUrl = this.exBackendURL + '/new';
    return this.httpClient.post <string>(postUrl, newName);
  }
  highlight(id) {
    const highlighted = document.getElementById(id);
    if (highlighted !== null) {
      if (highlighted.style.backgroundColor === 'red') {
        highlighted.style.backgroundColor = 'white';
        this.highlighted = null;
      } else {
        highlighted.style.backgroundColor = 'red';
        this.highlighted = id;
      }
      this.ref.detectChanges();
    }
  }
  checkHighlight(id) {
    if (this.highlighted === null) {
      this.highlight(id);
    } else {
      if (this.highlighted === id) {
        this.highlight(id);
      }
    }
  }
  removePost(): Observable<string> {
    const deleteUrl = this.exBackendURL + '/delete';
    return this.httpClient.post <string>(deleteUrl, this.highlighted);
  }
  removeElement() {
    const newElem = document.getElementById('new Element');
    if (this.highlighted !== null && newElem === null) {
      this.removePost().subscribe();
      this.updateView();
      this.highlighted = null;
    }
  }
  editElement() {
    const newElem = document.getElementById('new Element');
    if (this.highlighted !== null && newElem === null) {
      const hightlighted = document.getElementById('' + this.highlighted);
      const prevId = hightlighted;
      hightlighted.contentEditable = 'true';
      hightlighted.id = 'Edit Element';
      console.log(prevId.id);
      this.ref.detectChanges();
    }
  }


}



